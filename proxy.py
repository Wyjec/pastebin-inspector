import urllib2


def CheckProxy(ipPortList):
  for ip, port in ipPortList.items()[:20]:
    ipWithPort=ip+":"+port
    proxy = urllib2.ProxyHandler({'http': ipWithPort})
    opener = urllib2.build_opener(proxy)
    urllib2.install_opener(opener)
    try:
      response = urllib2.urlopen('http://www.wp.pl')
    except:
      continue
    if response.getcode() == 200 or response.getcode() == 301:
      return ipWithPort
