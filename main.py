from bs4 import BeautifulSoup
import urllib2
import time
from mail import Sendmail
from searchProxy import GetProxy
import unicodedata

#CONST
PASTEBIN_ARCHIVE='https://pastebin.com/archive'
PASTEBIN_URL='https://pastebin.com'

#CHANGEABLE
PHRASES=['@tlen.pl', '@o2.pl', '@wp.pl']
ADDITIONAL_DATA=['pass', 'login', 'haslo', 'smtp', 'user','credential', 'auth']

ipWithPort=""

def GetUrls():

  global ipWithPort

  #Try to find valid Proxy
  #ipWithPort=GetProxy()
  #if ipWithPort is not None:
  #  proxy = urllib2.ProxyHandler({'http': ipWithPort})
  #  opener = urllib2.build_opener(proxy)
  #  urllib2.install_opener(opener) 

  urls=[]
  ignoreLinks=['/messages', '/settings', '/scraping']

  try:
    html = urllib2.urlopen(PASTEBIN_ARCHIVE)
  except urllib2.URLError, e:
    print e
    return
    
  try:
    soup = BeautifulSoup(html, "lxml")
  except:
    soup = BeautifulSoup(html) 
    
  for link in soup.findAll("a"):
    singleLink = str(link.get('href'))
    if len(singleLink) == 9 and singleLink not in ignoreLinks:
      urls.append(PASTEBIN_URL+singleLink)
      
  SearchPhrases(*urls)


def SearchPhrases(*urls):
  #if ipWithPort is not None:
  #  proxy = urllib2.ProxyHandler({'http': ipWithPort})
  #  opener = urllib2.build_opener(proxy)
  #  urllib2.install_opener(opener) 

  for singleLink in urls:
    try:
      html = urllib2.urlopen(singleLink)
    except urllib2.URLError, e:
      print e
      continue
    try:
      soup = BeautifulSoup(html, "lxml")
    except:
      soup = BeautifulSoup(html)
    
    code=soup.find("textarea", {"id": "paste_code"})
    
    if code is None:
      continue

    code="".join(code.contents)
    if not isinstance(code, unicode):
      continue

    code = unicodedata.normalize('NFKD', code).encode('ascii','ignore')
    print singleLink
    if any(k in str(code) for k in PHRASES) and any(k in str(code) for k in ADDITIONAL_DATA):
      msg=singleLink.replace("https://","")

      if msg in open('pastebin.txt').read():
        continue
    
      Sendmail(msg+"\n\n\n"+code)

      with open ("pastebin.txt", "ab") as f:
        f.write("\n" + singleLink)

      print singleLink 
	
     
while True:
  GetUrls()
  time.sleep(180)