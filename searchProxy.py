from bs4 import BeautifulSoup
import urllib2
import re
import httplib
from proxy import CheckProxy

def GetProxy():
  try:
    conn = httplib.HTTPSConnection("free-proxy-list.net")
    conn.request("GET", "/")
    html = conn.getresponse();
  except httplib.HTTPException, e:
    print e
    return None
    
  try:
    soup = BeautifulSoup(html, "lxml")
  except:
    soup = BeautifulSoup(html) 

  data=[]

  table = soup.find('table', {'class': 'table table-striped table-bordered'})
  for row in table.findAll("td"):
    data.append(row.text)

  indices = [x for x in data if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", x) or re.match(r"^\d{2,5}$",x)]

  proxyList = dict(zip(indices[0::2], indices[1::2]))

  return CheckProxy(proxyList)
  